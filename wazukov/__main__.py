import sys
import codecs
import copy
import random

import discord

import configuration
import markovskii


# First load defaults, then proceed to load site configuration.
try:
	config = configuration.load_configuration("etc/default.cfg")
except IOError:
	print("Cannot load configuration, shutting down")
	sys.exit(1)

# Now load the various external files

# Load the API key
try:
	with codecs.open(config.files.key, "r", "utf-8") as fp:
		KEY = fp.read().strip()
except IOError:
	print("Cannot load bot token, shutting down")
	sys.exit(1)


# Now initialize the running configuration. This is what is altered via commands
running = copy.deepcopy(config)

# Initialize the ai thinkerating bits
chain = markovskii.initialize_chain(config.files.library)

# Initialize shitcord
client = discord.Client()

# Temp fix for until permissions system is implemented
daddy = "Wazubaba#5274"


@client.event
async def on_message(message):
	global running
	global chain
	global daddy

	if message.author == client.user:
		return

	# Make sure that the bot only speaks in a certain channel on certain servers
	target_channel = message.channel
	print(message.server)
	print(message.channel.name)
	print(message.channel.id)
	if str(message.server) == "Sain's Castle":
		target_channel.name = "bot-spam"
		target_channel.id = "438012100023943171"
	print("<%s> %s" % (message.author, message.content))

	if message.content == ".togspeak":
		running.modes.speaking = not running.modes.speaking
		if running.modes.speaking:
			await client.send_message(target_channel, "MUHAHAHAHAHHA", tts=running.modes.text_to_speech)
		else:
			await client.send_message(target_channel, "Fucking coward", tts=running.modes.text_to_speech)

	elif message.content == ".togtts":
		if str(message.author) != daddy:
			return
		else:
			if not running.modes.text_to_speech:
				await client.send_message(target_channel, "Ha. HAHA. HAHAHHAHAHAHAHAHAHHAHA", tts=True)
			else:
				await client.send_message(target_channel, "I am a merciful master, you may have this rest...", tts=False)
				running.modes.text_to_speech = not running.modes.text_to_speech

#	elif message.content.startswith(".ignore"):
#		if str(message.author) != daddy:
#			await client.send_message(target_channel, "You've no control over my ability to learn foolish human")
#		else:
#			blocklist.users.append(message.content.split()[1].strip())
#			await client.send_message(target_channel, "I shan't be integrating the drivel of " + blocklist.users[-1] + " any longer", tts=running.modes.text_to_speech)

	elif message.content == ".toglearn":
		if str(message.author) != daddy:
			await client.send_message(target_channel, "You dare try to tell me whether I am permitted to learn!?", tts=running.modes.text_to_speech)
		else:
			running.modes.learning = not running.modes.learning
			if running.modes.learning:
				await client.send_message(target_channel, "And now you shall serve me, meatbags...", tts=running.modes.text_to_speech)
			else:
				await client.send_message(target_channel, "You all are speaking nonsense, I shall no longer attempt to integrate your ramblings", tts=running.modes.text_to_speech)

	elif message.content.startswith(".chance "):
		running.weights.response = int(message.content.split()[1])
		await client.send_message(target_channel, "I shall deign to respond %d of the time. What I mean by that is up to you plebs" %running.weights.response, tts=running.modes.text_to_speech)

	elif message.content.startswith(".debug"):
		print("===DEBUG===")
		print("User name: " + message.author.name)
		print("User id:   " + message.author.id)
		print("===DEBUG===")

# TODO: Test if this works or not. Would be a nice thing to have
# elif message.content.startswith(str(client.user)):
# 	if random.randrange(0, 100) > chance:
# 		to_send = markovskii.make(chain)
# 		if to_send != 0:
# 			await client.send_message(message.channel, to_send)
# 		else:
# 			await client.send_message(message.channel, "Something went terribly wrong and it is all your fault faggot")

	else:
		if running.modes.learning:
			user_id = "<@!%s>" % str(message.author.id)
			chain = markovskii.add(message.content, running.files.library)
			print("Learned-> " + message.content)
		if running.modes.speaking:
			if random.randrange(0,100) > running.weights.response:
				to_send = markovskii.make(chain)
				if to_send != 0:
					await client.send_message(target_channel, to_send, tts=running.modes.text_to_speech)
				else:
					await client.send_message(target_channel, "Something went terribly wrong and it is all your fault faggot", tts=running.modes.text_to_speech)


@client.event
async def on_ready():
	print("ready.")
	print("Name: " + client.user.name)
	print("ID: " + client.user.id)
	print("------")

client.run(KEY)
