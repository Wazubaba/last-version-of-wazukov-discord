import json
import codecs
import logging
from typing import List

import discord


def load(path: str, client: discord.Client):
	try:
		with codecs.open(path, "r", "utf-8") as fp:
			rules = json.load(fp)
	except IOError:
		logging.error("Failed to open %s." % path)
		return None

	user_list = []

	for rule in rules:
		member = discord.utils.find(lambda m: m.name == rule["name"] or m.id == rule["id"], client.get_all_members())
		if member:
			user_list.append(member)

	return user_list


def append_user(user_list: List[discord.User], target_name: str, client: discord.Client):
	user = discord.utils.find(lambda u: u.name == target_name, client.get_all_members())
	if user:
		user_list.append(user)


def append_id(user_list: List[discord.User], target_id: int, client: discord.Client):
	user = discord.utils.find(lambda u: u.id == target_id, client.get_all_members())
	if user:
		user_list.append(user)


def save(path: str, user_list: List[discord.User]):
	data = []
	for user in user_list:
		data.append({
			"name": user.name,
			"id": user.id
		})

	with codecs.open(path, "w", "utf-8") as fp:
		json.dump(data, fp)
