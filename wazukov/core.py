import copy
import random
import codecs
import logging
import sys

import discord

import configuration
import server
import userlist
import markovskii


class Core (discord.Client):
	def __init__(self, config_path: str):
		super().__init__()
		self.settings = configuration.load_configuration(config_path)

		self.chain = markovskii.initialize_chain(self.settings.files.library)
		self.routing = None
		self.blocklist = None

		self.locked = False

		self.callbacks = {}

		self.routing = None
		self.blocklist = None
		self.admins = None

		self.running = copy.deepcopy(self.settings)

	async def on_ready(self):
		if self.settings.files.routing_rules:
			self.routing = server.load_rules(self.settings.files.routing_rules, self)

	#	if self.settings.files.blocklist_rules:
	#		self.blocklist = userlist.load(self.settings.files.blocklist_rules, self)

		if self.settings.files.adminlist_rules:
			self.admins = userlist.load(self.settings.files.adminlist_rules, self)

		print("ready.")
		print("Name: " + self.user.name)
		print("ID: " + self.user.id)
		print("------")

	# All we do here is the high-level communication driving stuff.
	async def on_message(self, message: discord.Message):

		# Ensure we are not responding to our own message
		if message.author == self.user:
			return

		# Test if the message is from someone on the blocklist
		if self.blocklist:
			if message.author in self.blocklist:
				return

		# Test if the message is a command and apply it if applicable.
		if message.content.startswith("."):
			if await self.try_command(message):
				return

		# Determine what server the message came from. If None do nothing
		if self.routing:
			server_rules = server.lookup(self.routing, message.server)
			if server_rules is None:
				return
		else:
			# We cannot do anything here so abandon processing
			return

		# Clean the message of any mentions
		self.sanitize(message)

		# Learn if applicable.
		if self.running.modes.learning:
			if message.channel in server_rules.learning_channels or server_rules.learning_channels == ["!ALL"]:
				self.chain = markovskii.add(message.content, self.running.files.library)
				print("Learned-> " + message.content)

		# Respond if applicable.
		if self.running.modes.speaking:
			target_channel = None

			# Value the channel activity was in over a random channel if permissions allow
			if message.channel in server_rules.prompter_channels or server_rules.prompter_channels == ["!ALL"]:
				if message.channel in server_rules.response_channels:
					target_channel = message.channel
				else:
					target_channel = random.choice(server_rules.response_channels)

			to_send = markovskii.make(self.chain)
			if to_send != 0:
				await self.send_message(target_channel, to_send, tts=self.running.modes.text_to_speech)

	def activate(self):
		try:
			with codecs.open(self.settings.files.key, "r", "utf-8") as fp:
				key = fp.read().strip()
		except IOError:
			logging.error("Cannot load api token, so cannot start")
			sys.exit(1)

		self.run(key)

	# Very simple helper function mostly for callback use
	async def say(self, channel: discord.Channel, message: str):
		await self.send_message(channel, message, tts=self.running.modes.speaking)

	@staticmethod
	def sanitize(message: discord.Message):
		#message.clean_content()
		new_message = message.content
		for item in message.raw_mentions:
			new_message.replace("@%d" % item.id, item.nick)

		message.content = new_message

	async def try_command(self, message: discord.Message):
		try:
			callback = None
			for key in self.callbacks:
				if message.content.startswith(key):
					callback = self.callbacks[key]

			print(callback)

			# Validate the number of args and level of the user.
			level = 0
			if message.server:
				if message.author.top_role.permissions.administrator:
					level = 2
				else:
					level = 1

			if self.admins:
				if message.author in self.admins:
					level = 3

			# Are we in locked-down mode?
			if level == 1 and self.locked:
				return True

			# Now that we know the user's level, test if they even have the right to this command
			if callback[1] <= level:
				# Test if we have enough args to satisfy minimum use case
				num_args = len(message.content.split()) - 1
				if num_args < callback[2]:
					# TODO: Handle not enough args to satisfy minimum
					await self.say(message.channel, "DEBUG: not enough args")
					return True

				# Whelp, we satisfied the minimum requirements, let's have the function figure it's shit out now.
				result = await callback[0](message, self)

				if result == 1:
					# TODO: Too many args
					await self.say(message.channel, "DEBUG: too many args")
					pass
				elif result == 2:
					# TODO: Malformed args
					await self.say(message.channel, "DEBUG: invalid arg(s) provided")
					pass
				elif result == 3:
					await self.say(message.channel, "DEBUG: I don't even fucking know anymore")
					# TODO: other error?
					pass

				# Either way - we definitely had a valid command here...
				return True

		except KeyError:
			return False
