# All commands will take the previous message and the client as arguments.

import discord
import core


async def show_help(message: discord.Message, bot: core.Core):
	if len(message.content.split()) > 1:
		return 1

	await bot.say(message.channel, "Commands:")
	await bot.say(message.channel, "  Level 1 commands")
	await bot.say(message.channel, "   `.help`: Show this help")
	await bot.say(message.channel, "   `.speak on|off|query`: Control whether the bot can speak, or see the current setting")
	await bot.say(message.channel, "   `.chance <number>|query`: Set the response chance or see its current value")
	await bot.say(message.channel, "  Level 2 commands")
	await bot.say(message.channel, "   `.leave_the_server`: Make the bot leave the current server")
	await bot.say(message.channel, "   `.lock on|off|query`: Lock out level 1 users or query the lock's status")
	await bot.say(message.channel, "   `.permissions add|remove|query learning|responding|speaking|all <channel>|!ALL`: Adjust local permissions or get a list of things. Omit the third argument if using query")
	await bot.say(message.channel, "   `.weight response|target|user|someone|everyone|release|query <value>`: Adjust or view the local weighting for the given type. Omit the third argument if using query")
	await bot.say(message.channel, "  Level 3 commands")
	await bot.say(message.channel, "   `.learn start|stop|query`: Control whether the bot can learn or see the current setting")
	await bot.say(message.channel, "   `.blocklist add|remove|query <user>`: Alter the blocklist entry for user or query if they are present. If the third argument is omitted then query will return a list of all blocked users")
	return 0


async def set_speak(message: discord.Message, bot: core.Core):
	args = message.content.split()
	if len(args) > 2:
		return 1

	if args[1].lower() == "on":
		bot.running.modes.speaking = True
	elif args[1].lower() == "off":
		bot.running.modes.speaking = False
	elif args[1].lower() == "query":
		await bot.say(message.channel, "Speaking mode: " + str(bot.running.modes.speaking))
	else:
		return 2

	return 0


async def set_chance(message: discord.Message, bot: core.Core):
	args = message.content.split()
	if len(args) > 2:
		return 1

	if args[1].lower() == "query":
		await bot.say(message.channel, "Chance to respond is " + str(bot.running.weights.response))

	elif args[1].isdigit():
		bot.running.weights.response = args[1]
		await bot.say(message.channel, "I shall now respond %d of the time, the meaning is up to you" % bot.running.weights.response)
	else:
		return 2

	return 0


async def leave_server(message: discord.Message, bot: core.Core):
	args = message.content.split()
	if len(args) > 1:
		return 1
	await bot.leave_server(message.server)


async def set_lock(message: discord.Message, bot: core.Core):
	args = message.content.split()
	if len(args) > 2:
		return 1

	if args[1].lower() == "on":
		bot.locked = True
		await bot.say(message.channel, "Activated lockout of level 1 users")
	elif args[1].lower() == "off":
		bot.locked = False
		await bot.say(message.channel, "Deactivated lockout of level 1 users")
	elif args[1].lower() == "query":
		await bot.say(message.channel, "Level 1 lockout is " + str(bot.locked))


async def set_permission(message: discord.Message, bot: core.Core):
	args = message.content.split()
	if len(args) > 4:
		return 1



	# Handle mode argument
	if args[1].lower() == "add":
		mode = 1
	elif args[1].lower() == "remove":
		mode = 2
	elif args[1].lower() == "query":
		mode = 3
	else:
		return 2

	# Handle target argument
	if args[2].lower() == "learning":
		target = 1
	elif args[2].lower() == "responding":
		target = 2
	elif args[2].lower() == "speaking":
		target = 3
	elif args[2].lower() == "all":
		target = 4
	else:
		return 2

	if mode == 3:
		if len(args) > 3:
			return 1

		if target == 1:
			bot.say(message.channel, "Channels learned from: " + str(bot.routing.learning_channels))
		elif target == 2:
			bot.say(message.channel, "Channels post-able to: " + str(bot.routing.response_channels))
		elif target == 3:
			bot.say(message.channel, "Channels that can trigger posts: " + str(bot.routing.prompter_channels))
		elif target == 4:
			bot.say(message.channel, "Channels learned from: " + str(bot.routing.learning_channels))
			bot.say(message.channel, "Channels post-able to: " + str(bot.routing.response_channels))
			bot.say(message.channel, "Channels that can trigger posts: " + str(bot.routing.prompter_channels))

		return 0

	if mode == 0 or target == 0:
		raise NameError

	# Handle final argument
	channel = None
	if args[3] == "!ALL":
		channel = discord.utils.find(lambda c: c.name == args[3], message.server.channels)

	# Append
	if mode == 1:
		if target == 1:
			bot.routing.push_learning(channel, args[3] == "!ALL")
		elif target == 2:
			bot.routing.push_response(channel, args[3] == "!ALL")
		elif target == 3:
			bot.routing.push_prompter(channel, args[3] == "!ALL")
		elif target == 4:
			bot.routing.push_learning(channel, args[3] == "!ALL")
			bot.routing.push_response(channel, args[3] == "!ALL")
			bot.routing.push_prompter(channel, args[3] == "!ALL")

	elif mode == 2:
		if target == 1:
			bot.routing.yank_learning(channel, args[3] == "!ALL")
		elif target == 2:
			bot.routing.yank_response(channel, args[3] == "!ALL")
		elif target == 3:
			bot.routing.yank_prompter(channel, args[3] == "!ALL")
		elif target == 4:
			bot.routing.yank_learning(channel, args[3] == "!ALL")
			bot.routing.yank_response(channel, args[3] == "!ALL")
			bot.routing.yank_prompter(channel, args[3] == "!ALL")

	else:
		return 3

	return 0


async def set_weight(message: discord.Message, bot: core.Core):
	args = message.content.split()
	if len(args) > 3:
		return 2

	if args[1].lower() == "query":
		if len(args) > 2:
			return 2

		bot.say(message.channel, "Weights:")
		bot.say(message.channel, "  Response: " + str(bot.running.weights.response))
		bot.say(message.channel, "  Targeted: " + str(bot.running.weights.targeted))
		bot.say(message.channel, "  User:     " + str(bot.running.weights.target_user))
		bot.say(message.channel, "  Someone:  " + str(bot.running.weights.target_someone))
		bot.say(message.channel, "  Everyone: " + str(bot.running.weights.target_global))
		bot.say(message.channel, "  Release:  " + str(bot.running.weights.drop_target))

		return 0

	try:
		value = int(args[2])
	except ValueError:
		return 2

	if args[1].lower() == "response":
		bot.running.weights.response = value
	elif args[1].lower() == "target":
		bot.running.weights.targeted = value
	elif args[1].lower() == "user":
		bot.running.weights.target_user = value
	elif args[1].lower() == "someone":
		bot.running.weights.target_someone = value
	elif args[1].lower() == "everyone":
		bot.running.weights.target_global = value
	elif args[1].lower() == "release":
		bot.running.weights.drop_target = value

	return 0


async def set_learn(message: discord.Message, bot: core.Core):
	args = message.content.split()
	if len(args) > 2:
		return 2

	if args[1] == "on":
		bot.running.modes.learning = True
	elif args[1] == "off":
		bot.running.modes.learning = False
	elif args[1] == "query":
		bot.say(message.channel, "Learning: " + "On" if bot.running.modes.learning else "Off")

	return 0

