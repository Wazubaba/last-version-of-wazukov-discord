
# This module handles processing of the routing_rules.json files.
# The intended purpose is to control how wazukov handles channels on a per-server basis

import json
import codecs
from typing import List

import dataclasses
import discord


@dataclasses.dataclass
class Rule:
	def __init__(self):
		self.server = ""  # Name of the server
		self.learning_channels = []  # List of channels that can be learned from
		self.response_channels = []  # List of channels that can be spoken into
		self.prompter_channels = []  # List of channels that can prompt a speech event

	def push_learning(self, channel: discord.Channel, is_all: bool):
		if is_all:
			self.learning_channels = ["!ALL"]
		else:
			if channel is None:
				raise ValueError
			self.learning_channels.append(channel)

	def push_response(self, channel: discord.Channel, is_all: bool):
		if is_all:
			self.response_channels = ["!ALL"]
		else:
			if channel is None:
				raise ValueError
			self.response_channels.append(channel)

	def push_prompter(self, channel: discord.Channel, is_all: bool):
		if is_all:
			self.prompter_channels = ["!ALL"]
		else:
			if channel is None:
				raise ValueError
			self.prompter_channels.append(channel)

	def yank_learning(self, channel: discord.Channel, is_all: bool):
		if is_all:
			self.learning_channels = []
		else:
			if channel is None:
				raise ValueError
			self.learning_channels.remove(channel)

	def yank_response(self, channel: discord.Channel, is_all: bool):
		if is_all:
			self.response_channels = []
		else:
			if channel is None:
				raise ValueError
			self.response_channels.remove(channel)

	def yank_prompter(self, channel: discord.Channel, is_all: bool):
		if is_all:
			self.prompter_channels = []
		else:
			if channel is None:
				raise ValueError
			self.prompter_channels.remove(channel)

# Use if Rule.learning_channels == ["!ALL"]: to test if we can do things anywhere or not


def load_rules(path: str, client: discord.Client):
	try:
		with codecs.open(path, "r", "utf-8") as fp:
			settings = json.load(fp)

	except IOError:
		print("server.load_rules: Failed to read file '%s'", path)
		return []

	except json.JSONDecodeError as err:
		print("server.load_rules: Failed to parse %s, line:%d col:%d %s" % (err.doc, err.lineno, err.colno, err.msg))
		raise err

	rules = []

	for server_name in settings:
		try:
			new_rule = Rule()


			# Figure out the server
			server = discord.utils.find(lambda s: s.name == server_name, client.servers)
			if not server:
				print("server.load_rules: Definition found for %s which we do not connect to!" % server_name)
				continue

			new_rule.server = server

			# Process learn-able channels
			if settings[server_name]["learning"] != ["!ALL"]:
				for channel_name in settings[server_name]["learning"]:
					channel = discord.utils.find(lambda c: c.name == channel_name, server.channels)
					if not channel:
						print("server.load_rules: Channel %s on Server %s does not exist, so cannot learn from it" % (channel_name, server_name))
						continue
					new_rule.learning_channels.append(channel)
			else:
				new_rule.learning_channels = settings[server_name]["learning"]

			# Process actionable channels
			if settings[server_name]["actionable"] != ["!ALL"]:
				for channel_name in settings[server_name]["actionable"]:
					channel = discord.utils.find(lambda c: c.name == channel_name, server.channels)
					if not channel:
						print("server.load_rules: Channel %s on Server %s does not exist, so cannot act on messages from it" % (channel_name, server_name))
						continue
					new_rule.prompter_channels.append(channel)
			else:
				new_rule.prompter_channels = settings[server_name]["actionable"]

			# Process respond-able channels
			if settings[server_name]["speakable"] != ["!ALL"]:
				for channel_name in settings[server_name]["speakable"]:
					channel = discord.utils.find(lambda c: c.name == channel_name, server.channels)
					if not channel:
						print("server.load_rules: Channel %s on Server %s does not exist, so cannot speak to it" % (channel_name, server_name))
						continue
					new_rule.response_channels.append(channel)
			else:
				new_rule.response_channels = settings[server_name]["speakable"]

			rules.append(new_rule)

		except KeyError as err:
			print("Error: missing key %s in rules for server %s. Skipping definition" % err.args)
			continue

	return rules


def save_rules(path: str, rules: List[Rule]):
	data = {}
	for rule in rules:
		server = str(rule.server)

		learning = []
		if rule.learning_channels != ["!ALL"]:
			for channel in rule.learning_channels:
				learning.append(str(channel))
		else:
			learning = rule.learning_channels

		actionable = []
		if rule.prompter_channels != ["!ALL"]:
			for channel in rule.prompter_channels:
				actionable.append(str(channel))
		else:
			actionable = rule.prompter_channels

		speakable = []
		if rule.response_channels != ["!ALL"]:
			for channel in rule.response_channels:
				speakable.append(str(channel))
		else:
			speakable = rule.response_channels

		data[server]["learning"] = learning
		data[server]["actionable"] = actionable
		data[server]["speakable"] = speakable

	with codecs.open(path, "w", "utf-8") as fp:
		json.dump(data, fp, indent="\t")


def lookup(rules: List[Rule], server: discord.Server):
	return discord.utils.find(lambda r: r.server == server, rules)
