import configparser
import codecs
import logging

import dataclasses

import defaults


# This module handles all configuration loading and writing.

@dataclasses.dataclass
class Weights:
	def __init__(self):
		self.response = 0
		self.targeted = 0
		self.target_user = 0
		self.target_someone = 0
		self.target_global = 0
		self.drop_target = 0


@dataclasses.dataclass
class Files:
	def __init__(self):
		self.key = ""
		self.library = ""
		self.routing_rules = ""
		self.blocklist_rules = ""
		self.adminlist_rules = ""


@dataclasses.dataclass
class Modes:
	def __init__(self):
		self.learning = False
		self.speaking = False
		self.text_to_speech = False


# This is the real settings container. Everything else just kinda is there to allow dot-notation access
@dataclasses.dataclass
class Settings:
	def __init__(self):
		self.files = Files()
		self.weights = Weights()
		self.modes = Modes()


# Now for things that actually do stuffs! \o/

def load_configuration(path: str):
	"""
	Load a target ini-formatted file and return a Settings object.
	:param path: Path to a target file.
	:type path: str
	:return: the settings
	:rtype: Settings
	"""

	# Initialize the container
	settings = Settings()

	# Initialize the parser
	config = configparser.ConfigParser()
	config.read(path, encoding="utf-8")

	# First try to read the files, since they are required
	if "files" not in config:
		logging.error("No files configured. Cannot continue.")
		raise KeyError

	# Now we iterate through and ensure all required values are present
	if "key file" not in config["files"]:
		logging.error("Missing required key 'key file' from 'files' section.")
		raise KeyError
	settings.files.key = config["files"]["key file"]

	if "library file" not in config["files"]:
		logging.error("Missing required key 'library file' from 'files' section.")
		raise KeyError
	settings.files.library = config["files"]["library file"]

	if "routing rules file" not in config["files"]:
		logging.warning("Missing key 'routing rules file' from 'files' section - Bot will not be able to do anything")

	if "adminlist file" not in config["files"]:
		logging.warning("Missing key 'adminlist file' from 'files' section - Bot will have unusable functions")

	settings.files.routing_rules = config["files"].get("routing rules file", None)
	settings.files.blocklist_rules = config["files"].get("blocklist file", None)
	settings.files.adminlist_rules = config["files"].get("adminlist file", None)

	# Ensure we have the required data
	if "modes" not in config:
		print("[Config] WARN: Using fallback values for modes")
		config["modes"] = {"Learning": defaults.DEFAULT_MODE_LEARNING,
											 "Speaking": defaults.DEFAULT_MODE_SPEAKING,
											 "Text To Speech": defaults.DEFAULT_MODE_TTS}

	# Grab the values, but in case the data is there but missing some elements still use fallback default values
	settings.modes.learning = config["modes"].getboolean("Learning", False)
	settings.modes.speaking = config["modes"].getboolean("Speaking", False)
	settings.modes.text_to_speech = config["modes"].getboolean("Text To Speech", False)

	# One more time
	if "weights" not in config:
		print("[Config] WARN: Using fallback values for weights")
		config["weights"] = {"Response Chance": defaults.DEFAULT_CHANCE_RESPONSE,
												 "Target Chance": defaults.DEFAULT_CHANCE_TARGET,
												 "User Chance": defaults.DEFAULT_CHANCE_USER,
												 "Someone Chance": defaults.DEFAULT_CHANCE_SOMEONE,
												 "Everyone Chance": defaults.DEFAULT_CHANCE_EVERYONE,
												 "Drop Target Chance": defaults.DEFAULT_CHANCE_DROP_TARGET}

	settings.weights.response = float(config["weights"].get("Response Chance", defaults.DEFAULT_CHANCE_RESPONSE))
	settings.weights.targeted = float(config["weights"].get("Target Chance", defaults.DEFAULT_CHANCE_TARGET))
	settings.weights.target_user = float(config["weights"].get("User Chance", defaults.DEFAULT_CHANCE_USER))
	settings.weights.target_someone = float(config["weights"].get("Someone Chance", defaults.DEFAULT_CHANCE_SOMEONE))
	settings.weights.target_global = float(config["weights"].get("Everyone Chance", defaults.DEFAULT_CHANCE_EVERYONE))
	settings.weights.drop_target = float(config["weights"].get("Drop Target Chance", defaults.DEFAULT_CHANCE_DROP_TARGET))

	# And voila~
	return settings


def write_configuration(path: str, settings: Settings):
	"""
	Write the given config to an ini-formatted file.
	:param path: Path to the file to write. Will overwrite the file if it exists, or create it otherwise.
	:type path: str
	:param settings: Settings object to write to the file.
	:type settings: Settings
	"""

	# Initialize the parser
	config = configparser.ConfigParser()

	# Construct the dicts for the config
	files = {
		"Key File": settings.files.key,
		"Library File": settings.files.library,
		"Blocklist File": settings.files.blocklist_rules,
		"Routing Rules File": settings.files.routing_rules,
		"Adminlist File": settings.files.adminlist_rules
	}

	modes = {
		"Learning": "yes" if settings.modes.learning else "no",
		"Speaking": "yes" if settings.modes.speaking else "no",
		"Text To Speech": "yes" if settings.modes.text_to_speech else "no"
	}

	weights = {
		"Response Chance": str(settings.weights.response),
		"Target Chance": str(settings.weights.targeted),
		"User Chance": str(settings.weights.target_user),
		"Someone Chance": str(settings.weights.target_someone),
		"Everyone Chance": str(settings.weights.target_global),
		"Drop Target Chance": str(settings.weights.drop_target)
	}

	config["files"] = files
	config["modes"] = modes
	config["weights"] = weights

	with codecs.open(path, "w", "UTF-8") as configFile:
		config.write(configFile)
