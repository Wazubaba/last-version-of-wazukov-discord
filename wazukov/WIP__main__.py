import core
import commands

print("Starting wazukov...")
core = core.Core("etc/default.cfg")

# Format is (callback, min_level, min_args_required)
core.callbacks = {
	".help": (commands.show_help, 1, 0),
	".speak": (commands.set_speak, 1, 1),
	".chance": (commands.set_chance, 1, 1),
	".lock": (commands.set_lock, 2, 1),
	".permissions": (commands.set_permission, 2, 2),
	".weight": (commands.set_weight, 2, 2),
	".learn:": (commands.set_learn, 3, 1)
}

core.activate()
