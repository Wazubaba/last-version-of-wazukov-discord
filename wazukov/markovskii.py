import random
from codecs import open


def initialize_chain(file_name):
	with open(file_name, "r", "utf-8") as fp:
		chain = {}
		first = "\n"
		second = "\n"

		for line in fp.readlines():
			for word in line.split():
				if word != "":
					chain.setdefault((first, second), []).append(word)
					first = second
					second = word

	return chain


def make(chain, maxw=25):
	try:
		temp = ""
		wordset = random.choice(list(chain))

		first, second = wordset

		for itr in range(random.randrange(1, maxw)):
			new = random.choice(chain[(first, second)])
			temp = temp + " " + new
			first = second
			second = new

		return temp
	except KeyError:
		return 0


def add(line, file_name):
	with open(file_name, "a", "utf-8") as fp:
		fp.write("\n" + line)

	return initialize_chain(file_name)
