
# This simply contains the defaults for the configuration system. You should be using a config instead of this file.

DEFAULT_MODE_LEARNING = "no"
DEFAULT_MODE_SPEAKING = "no"
DEFAULT_MODE_TTS = "no"

DEFAULT_CHANCE_RESPONSE = "85"
DEFAULT_CHANCE_TARGET = "25"
DEFAULT_CHANCE_USER = "75"
DEFAULT_CHANCE_SOMEONE = "20"
DEFAULT_CHANCE_EVERYONE = "5"
DEFAULT_CHANCE_DROP_TARGET = "35"
